-- project: plg-2-nka
-- file: Main.hs
-- author: Martin Štěpánek (xstepa59)
-- year: 2020

module Main where

import System.Environment
import System.Exit
import System.IO

import ParseInput
import Transform
import Types


-- Parses command line argument to get the mode of the program
parseMode :: String -> Maybe Mode
parseMode "-i" = Just LinearGram
parseMode "-1" = Just RegularGram
parseMode "-2" = Just FiniteAutomata
parseMode _ = Nothing


-- Does the main work according to the mode
doMainWork :: Maybe Mode -> [String] -> Result
doMainWork (Just LinearGram) input
    | Left rlg <- res = Result (show rlg) 0
    | Right error <- res = Result error 1
    where
        res = parse input

doMainWork (Just RegularGram) input
    | Left rrg <- res = Result (show rrg) 0
    | Right error <- res = Result error 1
    where
        res = linearToRegularGram . parse $ input

doMainWork (Just FiniteAutomata) input
    | Left rrg <- res = Result (show rrg) 0
    | Right error <- res = Result error 1
    where
        res = regGramToAutomata . linearToRegularGram . parse $ input

doMainWork Nothing _ = Result "Arguments error\n" 1


-- Parses command line arguments
parseCommandLine :: [String] -> (Maybe Mode, Maybe String)
parseCommandLine [mode] = (parseMode mode, Nothing)
parseCommandLine [mode, file] = (parseMode mode, Just file)
parseCommandLine _ = (Nothing, Nothing)


-- Reads input from stdin or file
getInput :: Maybe String -> IO String
getInput (Just file) = readFile file
getInput Nothing = getContents


printResult (Result res 0) = putStr res
printResult (Result res _) = hPutStr stderr res


-- Gets return code from the result data structure
getReturnCode :: Result -> ExitCode
getReturnCode (Result _ 0) = ExitSuccess
getReturnCode (Result _ code) = ExitFailure code


-- Main function
main = do
    args <- getArgs
    let (mode, file) = parseCommandLine args
    input <- getInput file
    let input_lines = lines input
    let result = doMainWork mode input_lines
    printResult result
    exitWith . getReturnCode $ result
