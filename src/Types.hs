-- project: plg-2-nka
-- file: Types.hs
-- author: Martin Štěpánek (xstepa59)
-- year: 2020

module Types where

import Utils


-- Mode of the program based on command line argument
data Mode = LinearGram | RegularGram | FiniteAutomata deriving (Enum)


-- Right linear grammar
data Rlg = Rlg {
     nonterm :: [Char],
     term :: [Char],
     start :: Char,
     rules :: [(Char, [Char])]
    } deriving (Eq)


-- Casts right linear grammar to string
instance Show Rlg where
    show rlg = unlines $
        [joinc "," $ nonterm rlg] ++
        [joinc "," $ term rlg] ++
        [[start rlg]] ++
        map (\(l, p) -> (l:'-':'>':p)) (rules rlg)


-- Right regular grammar
data Rrg = Rrg {
     nonterm' :: [String],
     term' :: [Char],
     start' :: String,
     rules' :: [(String, [String])]
    } deriving (Eq)


-- Casts right regular grammar to string
instance Show Rrg where
    show rrg = unlines $
        [joins "," $ nonterm' rrg] ++
        [joinc "," $ term' rrg] ++
        [start' rrg] ++
        map (\(l, p) -> l ++ ('-':'>':concat p)) (rules' rrg)


-- Nondeterministic finite automata
data Nfa = Nfa {
     states :: [Int],
     initial :: Int,
     finite :: [Int],
     transitions :: [(Int, Char, Int)]
    } deriving (Eq)


-- Casts nondeterministic finite automata to string
instance Show Nfa where
    show nfa = unlines $
        [joini "," $ states nfa] ++
        [show . initial $ nfa] ++
        [joini "," $ finite nfa] ++
        map (\(from, c, to) -> show from ++ [',', c, ','] ++ show to) (transitions nfa)


-- Result of the main function, containing result string and return code
data Result = Result String Int deriving(Eq)
