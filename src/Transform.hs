-- project: plg-2-nka
-- file: Transform.hs
-- author: Martin Štěpánek (xstepa59)
-- year: 2020

module Transform where

import Data.Char
import Data.List
import Data.Map (Map, findWithDefault)
import qualified Data.Map as Map

import Types
import Utils


-- Checks if string is a nonterminal
isNonterm :: String -> Bool
isNonterm (s:ss) = isAsciiUpper s


-- Finds states which should be marked as final
getFiniteStates :: [(String, [String])] -> Map String Int -> [Int]
getFiniteStates rules nontermToStates =
    map (\(l, r) -> findWithDefault 0 l nontermToStates) epsilonRules
    where
        epsilonRules = filter (\(l, r) -> r == ["#"]) rules


-- Transform one rule of regular grammar into transition of finite automata
ruleToTrans :: (String, [String]) -> Map String Int -> [(Int, Char, Int)]
ruleToTrans (_, ["#"]) _ = []
ruleToTrans (l, [[term], nonterm]) nontermToStates =
    [(from, term, to)]
    where
        from = findWithDefault 0 l nontermToStates
        to = findWithDefault 0 nonterm nontermToStates


-- Transform rules of regular grammar into transitions of finite automata
getTransitions :: [(String, [String])] -> Map String Int -> [(Int, Char, Int)]
getTransitions [] _ = []
getTransitions (r:rs) nontermToStates =
    ruleToTrans r nontermToStates ++ getTransitions rs nontermToStates


-- Creates mapping from nonterminals to states
mapNontermToStates :: [String] -> Map String Int
mapNontermToStates nonterm = Map.fromList (zip nonterm [0 ..])


-- Transforms regular grammar into finite automata
regGramToAutomata :: Either Rrg String -> Either Nfa String
regGramToAutomata (Right error) = Right error
regGramToAutomata (Left rrg) =
    Left Nfa {
        states = [0 .. length (nonterm' rrg) - 1],
        initial = findWithDefault 0 (start' rrg) nontermToStates,
        finite = sort finiteStates,
        transitions = getTransitions (rules' rrg) nontermToStates
    }
    where
        nontermToStates = mapNontermToStates $ nonterm' rrg
        finiteStates = getFiniteStates (rules' rrg) nontermToStates


-- Transforms one rule of linear grammar into the rules of regular grammar
ruleLinToReg :: (String, [String]) -> [String] -> ([(String, [String])], [String])
ruleLinToReg (l, []) nonterms = ([(l, ["#"])], nonterms)
ruleLinToReg (l, ["#"]) nonterms = ruleLinToReg (l, []) nonterms
ruleLinToReg (l, term:syms) nonterms
    | [sym] <- syms , isNonterm sym = ([(l, [term, sym])], nonterms)
    | otherwise = (new_rule:new_rules, new_nonterms)
    where
        new_nonterm = generateNonterm l nonterms
        new_rule = (l, [term, new_nonterm])
        (new_rules, new_nonterms) = ruleLinToReg (new_nonterm, syms) (nonterms ++ [new_nonterm])


-- Transforms rules of linear grammar into the rules of regular grammar
-- It first transorms the input rules into the correct format and that call
-- helper function rulesLinToReg'
rulesLinToReg :: [(Char, [Char])] -> [Char] -> ([(String, [String])], [String])
rulesLinToReg rules nonterm =
    rulesLinToReg' rules_in_rrg_format (map (:[]) nonterm)
    where
        rules_in_rrg_format = map (\(l, p) -> ([l], map (:[]) p)) rules


-- Transforms rules of linear grammar into the rules of regular grammar
rulesLinToReg' :: [(String, [String])] -> [String] -> ([(String, [String])], [String])
rulesLinToReg' [] nonterm = ([], nonterm)
rulesLinToReg' (r:rs) nonterm =
    (new_r ++ new_rs, nonterm'')
    where
        (new_r, nonterm') = ruleLinToReg r nonterm
        (new_rs, nonterm'') = rulesLinToReg' rs nonterm'


-- Given the list of nonterminals and one nonterminal from that list,
-- it generates new nonterminal starting with first letter of the given
-- nonterminal, followed be number. The number is computed as
-- 1 + highest number associated with the given letter so far.
-- Example: generateNonterm A1 ["A", "A1", "A2", "B", "B1", "B2", "B3"]
-- returns "A3" because highest number associated with A was 2 so far.
generateNonterm :: String -> [String] -> String
generateNonterm (s:_) nonterms = s : num
    where
        num = show $ 1 + generateNonterm' s nonterms


-- Finds highest suffix number used in nonterminals that start with the given letter
generateNonterm' :: Char -> [String] -> Int
generateNonterm' nterm [] = 0
generateNonterm' nterm ((sym1:syms):nonterms)
    | nterm /= sym1 = generateNonterm' nterm nonterms
    | otherwise = max (strToInt syms) (generateNonterm' nterm nonterms)


-- Transforms linear grammar into regular grammar
linearToRegularGram :: Either Rlg String -> Either Rrg String
linearToRegularGram (Right error) = Right error
linearToRegularGram (Left rlg) =
    Left Rrg {
        nonterm'=new_nonterm,
        term'=term rlg,
        start'=[start rlg],
        rules'=new_rules
    }
    where
        (new_rules, new_nonterm) = rulesLinToReg (rules rlg) (nonterm rlg)
