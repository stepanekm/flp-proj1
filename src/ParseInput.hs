-- project: plg-2-nka
-- file: ParseInput.hs
-- author: Martin Štěpánek (xstepa59)
-- year: 2020

module ParseInput where

import Data.Char

import Types
import Utils


-- Checks if all right sides of the rules are semantically correct
checkRightSideOfRule :: String -> [Char] -> [Char] -> Bool
checkRightSideOfRule "#" _ _ = True
checkRightSideOfRule [c] _ term = c `elem` term
checkRightSideOfRule "" _ _ = False
checkRightSideOfRule rightSide nonterm term = isOk
    where
        isOk = all (`elem` term) rightSide ||
            (all (`elem` term) (init rightSide) &&
            last rightSide `elem` nonterm)


-- Checks if linear grammar is semantically correct
checkRlgSemantics :: Rlg -> Either Rlg String
checkRlgSemantics rlg
    | isOk = Left rlg
    | not isOk = Right "The grammar is not correct.\n"
    where
        isOk = start rlg `elem` nonterm rlg &&
               all (\(l,p) -> l `elem` nonterm rlg) (rules rlg) &&
               all (\(l,p) -> checkRightSideOfRule p (nonterm rlg) (term rlg)) (rules rlg)


-- Parses string containing symbols delimited by comma
parseSymbols :: (Char -> Bool) -> String -> Maybe [Char]
parseSymbols checker line =
    mapM check_symbols symbols
    where
        symbols = map trim (split line ',')
        check_symbols [symbol]
            | checker symbol = Just symbol
            | otherwise = Nothing
        check_symbols _ = Nothing


-- Parse list of nonterminals
parseNonterm :: String -> Maybe [Char]
parseNonterm = parseSymbols isAsciiUpper


-- Parses list of terminals
parseTerm :: String -> Maybe [Char]
parseTerm = parseSymbols isAsciiLower


-- Parses starting nonterminal of the grammar
parseStartSymbol :: String -> Maybe Char
parseStartSymbol line
    | [startSymbol] <- symbols, isAsciiUpper startSymbol = Just startSymbol
    | otherwise = Nothing
    where
        symbols = trim line


-- Parses one rule of linear grammar
parseRule :: String -> Maybe (Char, String)
parseRule (left:'-':'>':right)
    | isAsciiUpper left = Just (left, right)
    | otherwise = Nothing
parseRule _ = Nothing


-- Parses rules of linear grammar
parseRules :: [String] -> Maybe [(Char, String)]
parseRules lines =
    sequence rules
    where
        rules = map parseRule lines


-- Parses linear grammar
parse :: [String] -> Either Rlg String
parse (l1:l2:l3:ls) =
    case (nonterm, term, start, rules) of
        (Just nonterm, Just term, Just start, Just rules) ->
            checkRlgSemantics Rlg {
                nonterm=nonterm,
                term=term,
                start=start,
                rules=rules
            }
        _ -> Right "The grammar is not correct.\n"
    where
        nonterm = parseNonterm l1
        term = parseTerm l2
        start = parseStartSymbol l3
        rules = parseRules ls

parse _ = Right "The grammar is not correct.\n"
