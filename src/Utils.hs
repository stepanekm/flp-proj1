-- project: plg-2-nka
-- file: Utils.hs
-- author: Martin Štěpánek (xstepa59)
-- year: 2020

module Utils where

import Data.Char


-- Helper function for joining values into string
-- Values are separated by given delimiter
join :: String -> [a] -> (a -> String) -> String
join _ [] _ = ""
join _ [x] toStr = toStr x
join del (x:xs) toStr = concat [toStr x, del, join del xs toStr]


-- Joins chars to one string, separated by delimiter
joinc :: String -> [Char] -> String
joinc del xs = join del xs (:[])


-- Joins strings to one string, separated by delimiter
joins :: String -> [String] -> String
joins del xs = join del xs id


-- Joins integers to one string, separated by delimiter
joini :: String -> [Int] -> String
joini del xs = join del xs show


-- Trims whitespaces from beggining and end of the string
trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace


-- Splits string into list of substrings based on the delimiter
split :: String -> Char -> [String]
split [] delim = [""]
split (x:xs) delim
   | x == delim = "" : rest
   | [] <- rest = [[x]]
   | (r:rs) <- rest = (x : r) : rs
   where
       rest = split xs delim


-- Reads integer from string. It returns 0 for empty string
strToInt :: String -> Int
strToInt [] = 0
strToInt s = read s :: Int
