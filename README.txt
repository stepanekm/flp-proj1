plg-2-nka

Author: Martin Štěpánek
Year: 2020

run: plg-2-nka [MODE] or plg-2-nka [MODE] INPUT
where MODE is one of the following:
    -i - Parses input linear grammar and prints it
    -1 - Transforms input linear grammar into regular grammar
    -2 - Transforms input linear grammar into nondeterministic finite automata

Result:
    If input grammar is correct, result is printed to stdout and program exits with code 0
    If error occurs, error message is printed to stderr and program exits with code 1

Notes:
    The program was checked with hlint tool. 10 hints were found - all of them suggested
    replacing [Char] with String. Nonetheless, [Char] is used in places where the type
    of variable is semantically really "list od chars", not "string", so that those
    hints were not obeyed.
