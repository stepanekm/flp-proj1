# project: plg-2-nka
# file: Makefile
# author: Martin Štěpánek (xstepa59)
# year: 2020

all: plg-2-nka

plg-2-nka: src/*.hs
	ghc src/Main.hs src/ParseInput.hs src/Transform.hs \
	src/Types.hs src/Utils.hs && mv src/Main ./plg-2-nka

clean:
	rm src/*.o src/*.hi plg-2-nka
